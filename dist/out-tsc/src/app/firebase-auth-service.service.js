import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var FirebaseAuthServiceService = /** @class */ (function () {
    function FirebaseAuthServiceService() {
    }
    FirebaseAuthServiceService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], FirebaseAuthServiceService);
    return FirebaseAuthServiceService;
}());
export { FirebaseAuthServiceService };
//# sourceMappingURL=firebase-auth-service.service.js.map