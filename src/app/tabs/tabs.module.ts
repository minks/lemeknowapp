import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
     { path: 'symptoms', loadChildren: '../symptoms/symptoms.module#SymptomsPageModule' },
      { path: 'allopathic', loadChildren: '../allopathic/allopathic.module#AllopathicPageModule' },
       { path: 'homeopathic', loadChildren: '../homeopathic/homeopathic.module#HomeopathicPageModule' },
       { path: 'ayurvedic', loadChildren: '../ayurvedic/ayurvedic.module#AyurvedicPageModule' }
  ]
},
{
  path:'',
  redirectTo:'/tabs/symptoms',
  pathMatch:'full'
}
  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
