import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllopathicPage } from './allopathic.page';

describe('AllopathicPage', () => {
  let component: AllopathicPage;
  let fixture: ComponentFixture<AllopathicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllopathicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllopathicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
