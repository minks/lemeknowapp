import { Component } from '@angular/core';
import { FirebaseService } from '../../pages/services/firebase.service';
//import { LoadingController } from 'ionic-angular';
import { AuthService } from '../../pages/services/auth.service';

@Component({
  selector: 'app-quicksearch',
  templateUrl: 'quicksearch.page.html',
  styleUrls: ['quicksearch.page.scss'],
})
export class QuickSearchPage {
  loading: any;
  searchTerm: string = '';
  searchResults: Array<any>;
  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService
  //  private loadingCtrl: LoadingController
  ) {
    //this.loading = this.loadingCtrl.create();
  }
searchEvent(){
  console.log("~~~~~Search Term" + this.searchTerm);
  this.getSearchResults();
}
  ionViewWillEnter(){
    console.log("~~~~~ login now ....");
    this.authService.doLogin(null);
    
  }

  getSearchResults(){
    this.firebaseService.getSearchResults()
    .then(results => {
      console.log("my first firebase read : " + results);
      this.searchResults = results;
    })
  }
  
}
