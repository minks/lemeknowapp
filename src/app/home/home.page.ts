import { Component, ViewChild,OnInit } from '@angular/core';
import { FirebaseService } from '../../pages/services/firebase.service';
//import { LoadingController } from 'ionic-angular';
import { AuthService } from '../../pages/services/auth.service';
import { IonApp, NavController } from '@ionic/angular';
import { QuickSearchPage } from '../quicksearch/quicksearch.page'
import { Location } from '@angular/common';
import { stringify } from '@angular/core/src/render3/util';
import { Router } from '@angular/router';

  
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements  OnInit  {
  // @ViewChild('searchbar') nav: NavController;
  quickSearchPage:any = QuickSearchPage;
  loading: any;
  searchTerm: string = '';
  searchResults: Array<any>;
  hideSearch = false;
  hidehome = true;
  sResult :string = '';
  
  Disease:any[]=[
    {id:1,name:"Vitiligo",url:'https://www.gstatic.com/webp/gallery3/1.png' ,
     desc :' Bacon ipsum dolor amet salami prosciutto ham hock, strip steak buffalo ribeye pork chop. Beef ribs tenderloin tail shoulder.Spare ribs ham shoulder brisket rump hamburger. Pork belly kevin shoulder prosciutto ribeye pork chop chicken strip steak pig.'},
     {id:2,name:"Vitiligo 1",url:'https://www.gstatic.com/webp/gallery3/1.png' ,
     desc :' Bacon ipsum dolor amet salami prosciutto ham hock, strip steak buffalo ribeye pork chop. Beef ribs tenderloin tail shoulder.Spare ribs ham shoulder brisket rump hamburger. Pork belly kevin shoulder prosciutto ribeye pork chop chicken strip steak pig.'},
     {id:1,name:"Vitiligo",url:'https://www.gstatic.com/webp/gallery3/1.png' ,
     desc :' Bacon ipsum dolor amet salami prosciutto ham hock, strip steak buffalo ribeye pork chop. Beef ribs tenderloin tail shoulder.Spare ribs ham shoulder brisket rump hamburger. Pork belly kevin shoulder prosciutto ribeye pork chop chicken strip steak pig.'}];
  constructor(
    private appCtlr: IonApp,
    private navCtrl: NavController,
    private firebaseService: FirebaseService,
    private authService: AuthService, private router:Router
    
    
  
  ) {
    //this.loading = this.loadingCtrl.create();
  }
  ngOnInit() {
    //this.onSearchCancelEvent(event);
   // alert('hi');
  }
onSearchEvent(){
  console.log("~~~~~Search Term" + this.searchTerm);
  this.hideSearch = true;
  this.hidehome = false;
  
}
  ionViewWillEnter(){
    console.log("~~~~~ login now ....");
    this.authService.doLogin(null);
    
  }

  getSearchResults(){
    this.firebaseService.getSearchResults()
    .then(results => {
      console.log("my first firebase read : " + results);
      this.searchResults = results;
    })
  }
  onSearchCancelEvent(event:any)
  {
    //this.router.navigate([''])
    this.searchTerm = '';
    this.hideSearch = false;
    this.hidehome = true;
  } 
  Backtohome()
  {
    this.searchTerm = '';
    this.hideSearch = false;
    this.hidehome = true;
  }
}
