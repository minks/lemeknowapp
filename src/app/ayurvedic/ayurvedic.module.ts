import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AyurvedicPage } from './ayurvedic.page';

const routes: Routes = [
  {
    path: '',
    component: AyurvedicPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AyurvedicPage]
})
export class AyurvedicPageModule {}
