import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AyurvedicPage } from './ayurvedic.page';

describe('AyurvedicPage', () => {
  let component: AyurvedicPage;
  let fixture: ComponentFixture<AyurvedicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AyurvedicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AyurvedicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
