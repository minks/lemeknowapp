import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeopathicPage } from './homeopathic.page';

describe('HomeopathicPage', () => {
  let component: HomeopathicPage;
  let fixture: ComponentFixture<HomeopathicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeopathicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeopathicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
